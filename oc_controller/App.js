import React, { Component} from 'react';
import { StyleSheet, Text, TextInput, View, Button} from 'react-native';
import Modal from 'react-native-modal';

let device_csn = '66666666';
let oc_list = {};
let server = '192.168.1.5:8200';

class Fetcher {

    async request(endpoint, body){
        let counstruct_url = function(endpoint){
            return `http://${server}/api/v1/device/oc/${endpoint}`;
        };
        let json_body = JSON.stringify(Object.assign({device: device_csn}, body));
        console.log(`Perform ${endpoint}, body: ${json_body}`);
        try {
            let response = await fetch(
                counstruct_url(endpoint),
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: json_body
                }
            );
            if (response.status == 200){
                let res = await response.json();
                return res;
            }
            return null;
        } catch (error) {
            console.log(error);
            return null;
        };
    };

    async getCollectors(){
        let res = await this.request('get_state');
        return res;
    };

    async setCollectorState(number, state){
        let base_params = {
            number: number,
            state: state,
            "open_interval": 1000,
            "close_interval": 1000
        };
        let res = await this.request(
            "set_state",
            {
                oc: base_params,
                "wait_result": false
            }
        );
    }
};

let fetcher = new Fetcher();

class TurningButton extends Component {
    render() {
        const type = this.props.type;
        const number = this.props.number;
        return (
            <Button
                onPress={async () => await fetcher.setCollectorState(number, type)}
                title={type}
            />
        );
    }
}

class CollectorEntity extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showModal: false
        };
    }

    toggleModal(){
        this.setState(
            prevState => ({showModal: !prevState.showModal})
        );
    }
    render(){
        let title = this.props.number + ': ' + this.props.type;
        return (
            <View>
              <Button
                  onPress={() => this.toggleModal()}
                  title={title}
              />
              <Modal
                  isVisible={this.state.showModal}
                  onBackButtonPress={() => this.toggleModal()}
                  onBackdropPress={() => this.toggleModal()}
              >
                <View style={styles.modalContent}>
                  <Text>Collector {this.props.number}</Text>
                  <View style={styles.controlButtons}>
                    <TurningButton number={this.props.number} type="open" />
                    <TurningButton number={this.props.number} type="close" />
                    <TurningButton number={this.props.number} type="pulse" />
                    <TurningButton number={this.props.number} type="periodic" />
                  </View>
                </View>
              </Modal>
            </View>
        );
    }
}

class Settings extends Component {

    constructor(props){
        super(props);
        this.state = {
            deviceCsn: device_csn,
            server: server,
            showModal: false
        };
    }
    changeDeviceCsn = (text) => this.setState({deviceCsn: text})
    changeServer = (text) => this.setState({server: text})

    toggleModal(){
        this.setState(
            prevState => ({showModal: !prevState.showModal})
        );
    }

    saveText(){
        device_csn = this.state.deviceCsn;
        server = this.state.server;
    }

    render() {
        return (
            <View>
              <Button
                  onPress={() => this.toggleModal()}
                  title="settings"
                  style={{color: 'darkred'}}
              />
                <Modal
                    isVisible={this.state.showModal}
                    onBackButtonPress={() => this.toggleModal()}
                    onBackdropPress={() => this.toggleModal()}
                >
                  <View style={styles.modalContent}>
                       <TextInput
                           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                           onChangeText={(text) => this.changeDeviceCsn(text)}
                           value={this.state.deviceCsn}
                       />
                       <TextInput
                           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                           onChangeText={(text) => this.changeServer(text)}
                           value={this.state.server}
                       />
                      <Button title="save" onPress={() => this.saveText()}/>
                     </View>
                </Modal>
            </View>
        )
    }
}


export default class MainView extends React.Component {
    constructor(props){
        super(props);
        this.state = {collectors: {}};
    };

    componentDidMount(){
        this.tickGetCollectors().then(
            () =>
                this.getColletorsTicker = setInterval(
                    async () => await this.tickGetCollectors(),
                    5000
                )
        );
    }

    componentWillUnmount(){
        if (this.getColletorsTicker !== undefinded){
            clearInterval(this.getColletorsTicker);
        }
    }

    async tickGetCollectors(){
        let res = await fetcher.getCollectors();
        this.setState({
            collectors: !!res ? res.oc : {}
        });
    }

  render() {
      let collectors = Object.keys(this.state.collectors).map(
          (number) => <CollectorEntity key={number} number={number} type={this.state.collectors[number]}/>
      );
      if (collectors.length == 0){
          collectors = <Text>Something wrong!</Text>;
      }
      return (
          <View style={styles.container}>
            {collectors}
            <Settings/>
          </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'skyblue',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
  },
  modalContent: {
      flexDirection: 'column',
      backgroundColor: 'white',
      padding: 22,
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  controlButtons: {
      flexDirection: 'row',
      justifyContent: 'space-between'
  }
});
