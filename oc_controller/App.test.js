import React from 'react';
import MainView from './App';

import renderer from 'react-test-renderer';

jest.useFakeTimers();

it('renders without crashing', () => {
    let mockedFetch = fetch.mockResponse(
        JSON.stringify(
            {oc: {1: 'close', 2: 'open'}}
        )
    );
    const component = renderer.create(<MainView />);
    expect(component.toJSON()).toMatchSnapshot();
    expect(mockedFetch).toBeCalled();
    jest.runAllTimers();
    expect(component.toJSON()).toMatchSnapshot();
});
