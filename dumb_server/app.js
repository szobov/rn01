var express = require('express');
var app = express();

app.post('/api/v1/device/oc/get_state', function (req, res) {
    console.log('get_state');
    res.json(
        {
            oc:
            {
                1: 'close',
                2: 'open',
                3: 'open',
                4: 'pulse'
            }
        });
});

app.post('/api/v1/device/oc/set_state', function (req, res) {
    console.log(req);
    res.json({status: 200});
});
app.listen(8200, '0.0.0.0', null, () => console.log('Example app listening on port 8200!'));
